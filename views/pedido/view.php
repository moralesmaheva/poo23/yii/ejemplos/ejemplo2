<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = "id: " . $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedido-view">

    <h1><?= Html::encode("Modificando el pedido: " . $this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta seguro de eliminar este pedido?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'total',
            'fecha',
            'id_cliente',
            //sacar datos relacionales de otra tabla
            //opcion 1
            //'cliente.nombre',
            //opcion 2
            [
                'label' => 'Nombre del cliente',
                'value' => function ($model) {
                    return "{$model->cliente->nombre} {$model->cliente->apellido1} {$model->cliente->apellido2}";
                }
            ],

            'id_comercial',
            'comercial.nombre',
        ],
    ]) ?>

</div>