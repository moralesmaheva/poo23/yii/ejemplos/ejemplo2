<?php

use app\models\Pedido;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Gestion de Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'total',
            'fecha',
            'id_cliente',
            //para utilizar una relacion
            //opcion 1
            //coloco el nombre de la relacion (que esta en el getter del modelo)
            // mas . y el nombre del campo a mostrar
            'cliente.nombre',

            //opcion 2
            //creando un array con la etiqueta y el nombre del campo
            [
                'label' => 'Nombre del cliente',
                'value' => 'cliente.nombre'
            ],
            //coloco el nombre de la relacion (que esta en el getter del modelo)
            // mas . y el nombre del campo a mostrar
            'comercial.ciudad',
            'id_comercial',

            [
                'label' => 'Nombre del Comercial',
                'value' => function ($modelo) {
                    return "{$modelo->comercial->nombre} {$modelo->comercial->apellido1} {$modelo->comercial->apellido2}";
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pedido $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>