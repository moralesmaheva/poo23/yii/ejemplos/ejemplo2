<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Comercial $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Comercials', 'url' => ['index']];
$this->params['breadcrumbs'][] = "Id: " . $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="comercial-view">

    <h1><?= Html::encode("Modificar el comercial con Id: " . $this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'apellido1',
            'apellido2',
            'comisión',
        ],
    ]) ?>

</div>