<div class="card">
    <div class="card-body">
        <h5 class="card-title">Comercial <?= $model->id ?></h5>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Nombre: <?= $model->nombre ?></li>
        <li class="list-group-item">Primer apellido: <?= $model->apellido1 ?></li>
        <li class="list-group-item">Segundo apellido: <?= $model->apellido2 ?></li>
        <li class="list-group-item">Comision: <?= $model->comisión ?></li>
    </ul>
</div>