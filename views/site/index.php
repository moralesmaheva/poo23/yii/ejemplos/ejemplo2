<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Gestion de Pedidos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gestion del almacen</h1>

        <p class="lead">Gestion de Comerciales, Clientes y Pedidos</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 mb-3 border rounded">
                <h2 class="text-center">Clientes</h2>
                <p class="text-center"><?= Html::a('Ver Clientes', ['/cliente/index'], [
                                            'class' => 'btn btn-primary'
                                        ]) ?></p>
            </div>
            <div class="col-lg-4 mb-3 border rounded">
                <h2 class="text-center">Comerciales</h2>
                <p class="text-center"><?= Html::a('Ver Comerciales', ['/comercial/index'], [
                                            'class' => 'btn btn-primary'
                                        ]) ?></p>
            </div>
            <div class="col-lg-4 mb-3 border rounded">
                <h2 class="text-center">Pedidos</h2>
                <p class="text-center"><?= Html::a('Ver Pedidos', ['/pedido/index'], [
                                            'class' => 'btn btn-primary'
                                        ]) ?></p>
            </div>
        </div>

    </div>
</div>